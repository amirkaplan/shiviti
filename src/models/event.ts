import { Time } from "@angular/common";

export enum EventType {
    shaharit, 
    mincha, 
    arvit    
}

export class _Event {
    Date: Date;
    Time: Time;
    type: EventType;
}