import { _Event } from "./event";

export enum PlaceType {
    synagogue    
}

export class Place {
    name: string = '';
    address: string = '';
    lat: number = 0;
    lng: number = 0;
    place_id: string;
    type: PlaceType = PlaceType.synagogue;
    events: _Event[];
}