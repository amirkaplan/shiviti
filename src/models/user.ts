
  // The account fields for the login form.
  export interface User {
      email: string;
      password: string;
  }