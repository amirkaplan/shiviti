import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPlacePage } from './add-place';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddPlacePage,
  ],
  imports: [
    IonicPageModule.forChild(AddPlacePage),
    TranslateModule.forChild()
  ],
})
export class AddPlacePageModule {}
