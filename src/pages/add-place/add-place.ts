import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { Place } from '../../models/place';
import { PlacesProvider } from '../../providers/places/places';
import { google } from "google-maps";
import { PlacesAutocompletePage } from '../places-autocomplete/places-autocomplete';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare var google: any
@IonicPage()
@Component({
  selector: 'page-add-place',
  templateUrl: 'add-place.html',
})
export class AddPlacePage {

  //place: Place = new Place();
  name;
  address;
  lat;
  lng;
  autocompleteItems;
  autocomplete;
  form: FormGroup;;

  placesService = new google.maps.places.AutocompleteService();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private placesProvider: PlacesProvider,
    private modalCtrl: ModalController,
    private formBuilder: FormBuilder) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };

    this.form = this.formBuilder.group({
      name: [''],
      address: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddPlacePage');
  }

  updateSearch() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let me = this;
    this.placesService.getPlacePredictions({ input: this.autocomplete.query }, function (predictions, status) {
      me.autocompleteItems = [];
      if (predictions != null) {
        predictions.forEach(function (prediction) {
          me.autocompleteItems.push(prediction.description);
        });
      }
    });
  }

  chooseItem(item: any) {
    this.address = item;
    this.geoCode(item);
    this.autocomplete.query = '';
    this.autocompleteItems = [];
  }

  dismiss() {
    this.autocompleteItems = [];
  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      this.lat = results[0].geometry.location.lat();
      this.lng = results[0].geometry.location.lng();
    });
  }

  save(place: Place) {
    if (this.lat != null && this.lng != null) {
      var p = {
        place_id: `${this.lat}_${this.lng}`,
        name: this.name,
        lat: this.lat,
        lng: this.lng
      }
      console.log(p);
      this.placesProvider.addPlace(p).then((res: any) => {
        if (!res.code) {
          this.navCtrl.setRoot('TabsPage');
        } else {
          alert(res);
        }
      });
    }
  }
}
