import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../models/user' 
import { AuthProvider } from '../../providers/auth/auth';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  // user = {} as User;
  user: { email: string, password: string } = {
    email: 'amirkaplan@gmail.com',
    password: 'qweasdzxc'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  signin(user: User){
    this.authService.login(this.user).then((res: any) => {
      if (!res.code){
        this.navCtrl.setRoot('TabsPage');
      } else {
        alert(res);
      }
    });
  }

}
