import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';

@IonicPage()
@Component({
  selector: 'page-password-recovery',
  templateUrl: 'password-recovery.html',
})
export class PasswordRecoveryPage {

  email: string = 'amirkaplan@gmail.com';

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserProvider,
              public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PasswordRecoveryPage');
  }

  sendResetPasswordEmail() {
    let alert = this.alertCtrl.create({ 
      buttons: ['ok']
     }) 
    this.userService.recoverPassword(this.email).then((res: any) => {
      if (res.success){
        alert.setTitle('המייל נשלח בהצלחה');
        alert.setSubTitle('נשלח מייל עם קישור לאיפוס הסיסמא');
      } else {
        alert.setTitle('שליחת המייל נכשלה');
        alert.setSubTitle('נא נסו שוב');      }
    });
  }

}
