import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlacePage } from './place';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    PlacePage,
  ],
  imports: [
    IonicPageModule.forChild(PlacePage),
    TranslateModule.forChild()
  ]
})
export class PlacePageModule {}
