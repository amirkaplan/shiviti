import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlacesAutocompletePage } from './places-autocomplete';

@NgModule({
  declarations: [
    PlacesAutocompletePage,
  ],
  imports: [
    IonicPageModule.forChild(PlacesAutocompletePage),
  ],
})
export class PlacesAutocompletePageModule {}
