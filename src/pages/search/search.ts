import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation, GeolocationOptions, Geoposition } from '@ionic-native/geolocation';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  LatLng,
  HtmlInfoWindow
} from '@ionic-native/google-maps';
import { Place } from '../../models/place';
import { PlacePage } from '../place/place';
import { PlacesProvider } from '../../providers/places/places';
import { AngularFirestore } from 'angularfire2/firestore';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  options: GeolocationOptions;
  currentPos: Geoposition;

  @ViewChild('map_canvas') map_canvas: HTMLDivElement;
  map: GoogleMap;
  lat;
  lng;
  autocompleteItems;
  autocomplete;

  places = [];

  placesService = new google.maps.places.AutocompleteService();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private googleMaps: GoogleMaps,
    private geolocation: Geolocation,
    private placesProvider: PlacesProvider,
    private db: AngularFirestore
  ) {
    this.autocompleteItems = [];
    this.autocomplete = {
      query: ''
    };
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  initialMapLoad: boolean = true;

  ngAfterViewInit() {
    this.getUserPosition();
  }

  ionViewWillEnter() {
    if (!this.initialMapLoad) {
      this.map.setDiv('map_canvas');
      this.map.setVisible(true);
      setTimeout(() => {
        // hack to fix occasionally disappearing map
        this.map.setDiv('map_canvas');
      }, 1000);
    } else {
      this.initialMapLoad = false;
    }
  }

  ionViewWillLeave() {
    // unset div & visibility on exit
    this.map.setVisible(false);
    this.map.setDiv(null);
  }

  loadMap(lat, lng) {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: lat,
          lng: lng
        },
        zoom: 14,
        tilt: 30
      }
    };
    this.map = GoogleMaps.create('map_canvas', mapOptions);
    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');
        let latLng = new google.maps.LatLng(lat, lng);
        var sw = this.map.getVisibleRegion().southwest;
        var ne = this.map.getVisibleRegion().northeast;
        this.getSynagogues(latLng, sw, ne);
      });

    // let watch = this.geolocation.watchPosition();
    //   watch.subscribe((data) => {
    //     this.lat = data.coords.latitude;
    //     this.lng = data.coords.longitude;
    //     this.animateCamera();
    //   });
  }

  getUserPosition() {
    this.options = {
      enableHighAccuracy: false
    };
    this.geolocation.getCurrentPosition(this.options).then((pos: Geoposition) => {
      this.currentPos = pos;
      this.loadMap(pos.coords.latitude, pos.coords.longitude);
    }, (err: PositionError) => {
      console.log("error : " + err.message);
    });
  }

  addMarker(place) {
    var index = this.places.findIndex(p => p.place_id == place.place_id);
    if (index < 0) {
      this.map.addMarker({
        icon: 'blue',
        animation: 'DROP',
        draggable: true,
        disableAutoPan: true,
        position: {
          lat: place.lat,
          lng: place.lng
        }
      }).then(marker => {
        place.marker = marker;
        place.infoWindow = this.setInfoWindow(place);
        this.places.push(place);

       // marker.showInfoWindow();

        marker.on(GoogleMapsEvent.MARKER_CLICK)
          .subnfoscribe(() => {

            this.onMarkerClick(marker)
            // let htmlInfoWindow = new HtmlInfoWindow();
            // let frame: HTMLElement = document.createElement('div');
            // frame.innerHTML = [
            //   '<h3>Hearst Castle</h3>', '<h3>Hearst Castle</h3>'
            // ].join("");
            // htmlInfoWindow.setContent(frame, {width: "280px", height: "330px"});
            
            // htmlInfoWindow.open(marker);

            // let infoWindow = new HtmlInfoWindow();
            // let frame: HTMLElement = document.createElement('div');
            // frame.innerHTML = `<h3>${place.name}</h3>`;
            // infoWindow.setContent(frame);
            // infoWindow.open(marker);
           
            // this.navCtrl.push(PlacePage, {
            //   place: place
            // });
         
          });
      });
    }
  }

  // onMarkerClick(params: any[]) {
  //   // Get a marker instance from the passed parameters
  //   let marker: Marker = params.pop();
  
  //   // Create a component
  //   const compFactory = this.resolver.resolveComponentFactory(CustomTag);
  //   let compRef: ComponentRef<CustomTag> = compFactory.create(this.injector);
  //   compRef.instance.myTitle = marker.get('myTitle');
  //   this.appRef.attachView(compRef.hostView);
  
  //   let div = document.createElement('div');
  //   div.appendChild(compRef.location.nativeElement);
  
  //   // Dynamic rendering
  //   this._ngZone.run(() => {
  //     this.htmInfoWindow.setContent(div);
  //     this.htmInfoWindow.open(marker);
  //   });
  // }
  


  setInfoWindow(place) {
    let infoWindow = new HtmlInfoWindow();
    let frame: HTMLElement = document.createElement('div');
    frame.innerHTML = `<h3>${place.name}</h3>`;
    infoWindow.setContent(frame);
    return infoWindow;
  }


  getSynagogues(latLng, sw, ne) {
    var me = this;
    this.placesProvider.getSynagogues(latLng).then((results: Array<any>) => {
      var collectionRef = this.db.collection("places").ref;
      collectionRef.where("lat", ">", sw.lat)
        //.where("lng", ">", sw.lng)
        .where("lat", "<", ne.lat)
        //.where("lng", "<", ne.lng)
        .get()
        .then(function (querySnapshot) {
          results.forEach(function (place) { // loop over google places
            var index = querySnapshot.docs.findIndex(d => d.id == place.place_id);
            if (index < 0) {
              // place in google not in db
              me.placesProvider.addPlaceFromGoogle(place).then((new_place) => {
                me.addMarker(new_place)
              });
            } else {
              me.addMarker(querySnapshot.docs[index].data());
            }
          });

          querySnapshot.forEach(function (doc) { // loop over db places
            var d = doc.data();
            if (d.lng > sw.lng && d.lng < ne.lng) {
              var index = results.findIndex(r => r.place_id == doc.id);
              if (index < 0) {
                // place in db not in google
                me.addMarker(doc.data());
              }
            }
          });
        })
        .catch((error) => { console.log("Error getting documents: ", error); });
    }).catch((error) => { console.log("Error getSynagogues: ", error); });
  }

  updateSearch() {
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let me = this;
    this.placesService.getPlacePredictions({ input: this.autocomplete.query }, function (predictions, status) {
      me.autocompleteItems = [];
      if (predictions != null) {
        predictions.forEach(function (prediction) {
          me.autocompleteItems.push(prediction.description);
        });
      }
    });
  }

  chooseItem(item: any) {
    this.geoCode(item);
    this.autocomplete.query = '';
    this.autocompleteItems = [];
  }

  dismiss() {
    this.autocompleteItems = [];
  }

  geoCode(address: any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results, status) => {
      let latLng = new LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());
      let position = {
        target: latLng,
        zoom: 15,
        duration: 700
      };
      this.map.animateCamera(position).then(() => {

        var sw = this.map.getVisibleRegion().southwest;
        var ne = this.map.getVisibleRegion().northeast;
        this.getSynagogues(latLng, sw, ne);

      }).catch((error) => { console.log(error) })
    });
  }
}
