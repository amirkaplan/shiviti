import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
 
  // The account fields for the login form.
  user: { name: string, email: string, password: string } = {
    name: 'amir kaplan',
    email: 'amirkaplan@gmail.com',
    password: 'qweasdzxc'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public userService: UserProvider,
              public loading: LoadingController, public toast: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  signup() {
    if (this.user.email == '' || this.user.password == '' || this.user.name == ''){
      var t = this.toast.create({
        duration: 3000,
        position: 'bottom',
        message: 'נא למלא את כל השדות'
      });
      t.present();
    }
    let loader = this.loading.create({
      content: 'טוען'
    });
    loader.present();
    this.userService.addUser(this.user).then((res: any) => {
      loader.dismiss();
      if (res.success){
        this.navCtrl.setRoot('ProfileDetailsPage');
      } else {
        alert(res);
      }
    })
  }
}
