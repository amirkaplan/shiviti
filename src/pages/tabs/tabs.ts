import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  SearchTab: string = 'SearchPage';
  AddTab: string = 'AddPlacePage'
  ChatTab: string = 'ChatPage';
  ProfileTab: string = 'ProfilePage';

  constructor() {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
