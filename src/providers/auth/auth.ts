import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../models/user' 

@Injectable()
export class AuthProvider {
  constructor(public fireAuth: AngularFireAuth) {
    console.log('Hello AuthProvider Provider');
  }

  login(user: User) {
    var promise = new Promise((resolve, reject) =>
      this.fireAuth.auth.signInWithEmailAndPassword(user.email, user.password).then(() => {
        resolve(true);
      }).catch((err) => { 
        reject(err);
      })
    )
    return promise;
  }
}
