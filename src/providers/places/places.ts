import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Place } from '../../models/place';

@Injectable()
export class PlacesProvider {

  constructor(public http: HttpClient, private db: AngularFirestore) {
    console.log('Hello PlacesProvider Provider');
  }

  getSynagogues(latLng) {

    var m = document.getElementById('map_canvas') as HTMLDivElement;
    var service = new google.maps.places.PlacesService(m);
    let request = {
      location: latLng,
      radius: 8047,
      types: ["synagogue"]
    };

    var promise =  new Promise((resolve, reject) => {
      var me = this;
      service.nearbySearch(request, function (results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          resolve(results);
        } else {
          reject(status);
        }
      })
    })
    return promise;
  }

  addPlaceFromGoogle(place) {
      return this.addPlace({
        place_id: place.place_id,
        name: place.name,
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng()
      });
  }

  addPlace(place) {
    var promise = new Promise((resolve, reject) => {
      var p = {
        place_id: place.place_id,
        name: place.name,
        lat: place.lat,
        lng: place.lng
      };
      this.db.collection('places').doc(p.place_id).set(p).then(() => {
        resolve(p);
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }


  getPlaces() {

  }

}
