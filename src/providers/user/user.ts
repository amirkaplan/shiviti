import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable()
export class UserProvider {

  constructor(public fireAuth: AngularFireAuth, private db: AngularFirestore) {
    console.log('Hello UserProvider Provider');
  }

  addUser(user) {
    var promise = new Promise((resolve, reject) => {
      this.fireAuth.auth.createUserWithEmailAndPassword(user.email, user.password).then(() => {
        this.fireAuth.auth.currentUser.updateProfile({
          displayName: user.name,
          photoURL: ''
        }).then(() => {
          this.db.collection('users').doc(this.fireAuth.auth.currentUser.uid).set({
            uid: this.fireAuth.auth.currentUser.uid,
            displayName: user.name,
            photoURL: 'https://www.google.co.il/imgres?imgurl=https%3A%2F%2Fwww.bridginghearts.org%2Fimages%2Fhearts.png&imgrefurl=https%3A%2F%2Fwww.bridginghearts.org%2F&docid=AT8Qk-kdkB1prM&tbnid=TFKaValnjrXw1M%3A&vet=10ahUKEwi5huuIxvPYAhXDZFAKHTuvAS8QMwhJKBAwEA..i&w=256&h=256&safe=off&bih=826&biw=1440&q=heart&ved=0ahUKEwi5huuIxvPYAhXDZFAKHTuvAS8QMwhJKBAwEA&iact=mrc&uact=8'
          }).then(() => {
            resolve({ success: true });
          }).catch((err) => {
            reject(err);
          })
        }).catch((err) => {
          reject(err);
        })
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }

  recoverPassword(email) {
    var promise = new Promise((resolve, reject) => {
      this.fireAuth.auth.sendPasswordResetEmail(email).then(() => {
        resolve({ success: true });
      }).catch((err) => {
        reject(err);
      })
    })
    return promise;
  }


}
